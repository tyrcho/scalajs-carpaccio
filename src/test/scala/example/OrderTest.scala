package example

import org.scalatest._

class OrderTest extends FlatSpec with Matchers {
  "an Order" should "compute final price" in {
    Order(123, 39.99, State.UT).finalPrice shouldBe 5098.03 +- 0.01
    Order(1235, 49.99, State.AL).finalPrice shouldBe 54576.08 +- 0.01
    Order(95, 19.99, State.CA).finalPrice shouldBe 1994.05 +- 0.01
    Order(50, 23, State.NV).finalPrice shouldBe 1204.74 +- 0.01
  }

}
