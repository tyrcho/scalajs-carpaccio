package example

import org.scalajs.dom.html.TableRow
import org.scalajs.dom.raw.HTMLElement

import scalatags.JsDom
import scalatags.JsDom.all._

object Views {
  def makeRow(name: String, elt: HTMLElement): JsDom.TypedTag[TableRow] =
    tr(th(name, `class` := "col-xs-1"), td(elt, `class` := "col-xs-1"))

  def makeTable(rows: JsDom.TypedTag[TableRow]*) =
    table(`class` := "table table-condensed", tbody(rows))

  def formatPrice(price: Double): String = f"$$ $price%.2f"

  def formatPercent(percent: Double): String = f"${percent * 100}%.2f %%"
}
