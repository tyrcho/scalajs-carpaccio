package example
import japgolly.scalajs.react.ScalaComponent
import japgolly.scalajs.react.vdom.html_<^._

object LinkComponent {

  case class LinkData(text: String, url: String)

  def apply(props: LinkData) =
    ScalaComponent
      .builder[LinkData]("link")
      .render_P(ld => <.a(ld.text, ^.href := ld.url))
      .build
      .apply(props)

}
