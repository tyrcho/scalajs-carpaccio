package example

import example.State.State

case class Order(items: Int, price: Double, state: State = State.UT) {
  def totalPrice: Double = items * price

  def discountRate: Double =
    Discount.values.toSeq
      .sortBy(-_.minPrice)
      .find(_.minPrice <= totalPrice)
      .map(_.rate)
      .getOrElse(0)

  def discountAmount: Double = discountRate * totalPrice

  def discountedPrice: Double = totalPrice - discountAmount

  def taxRate: Double = state.rate

  def taxAmount: Double = taxRate * discountedPrice

  def finalPrice: Double = discountedPrice + taxAmount
}

object Discount extends Enumeration {
  type Discount = Value

  case class DiscountValue(minPrice: Double, rate: Double) extends super.Val(minPrice.toString)

  DiscountValue(1000, 0.03)
  DiscountValue(5000, 0.05)
  DiscountValue(7000, 0.07)
  DiscountValue(10000, 0.1)
  DiscountValue(50000, 0.15)

  implicit def convert(value: Value): DiscountValue = value.asInstanceOf[DiscountValue]
}

object State extends Enumeration {
  type State = Value

  case class StateValue(name: String, rate: Double) extends super.Val(name) {
    def description = f"$name (${rate * 100}%.2f %%)"
  }

  val UT = StateValue("UT", 0.0685)
  val NV = StateValue("NV", 0.08)
  val TX = StateValue("TX", 0.0625)
  val AL = StateValue("AL", 0.04)
  val CA = StateValue("CA", 0.0825)

  implicit def convert(value: Value): StateValue = value.asInstanceOf[StateValue]
}
