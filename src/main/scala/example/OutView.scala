package example

import example.Views._
import org.scalajs.dom.html._
import org.scalajs.dom.raw._

import scalatags.JsDom.all._

object OutView {
  private val outUnitPrice: Span = span().render
  private val outItems: Span = span().render
  private val outTotalPrice: Span = span().render
  private val outDiscountRate: Span = span().render
  private val outDiscountAmount: Span = span().render
  private val outDiscountedPrice: Span = span().render
  private val outTaxRate: Span = span().render
  private val outTaxAmount: Span = span().render
  private val outFinalPrice: Span = span().render

  def displayOrder(o: Order): Unit = {
    outItems.textContent = o.items.toString
    outUnitPrice.textContent = formatPrice(o.price)
    outTotalPrice.textContent = formatPrice(o.totalPrice)
    outDiscountRate.textContent = formatPercent(o.discountRate)
    outDiscountAmount.textContent = formatPrice(o.discountAmount)
    outDiscountedPrice.textContent = formatPrice(o.discountedPrice)
    outTaxRate.textContent = formatPercent(o.taxRate)
    outTaxAmount.textContent = formatPrice(o.taxAmount)
    outFinalPrice.textContent = formatPrice(o.finalPrice)
  }

  def render: Div = {
      div(
        makeTable(
          makeRow("Items", outItems),
          makeRow("Unit price", outUnitPrice),
          makeRow("Total price", outTotalPrice)),
        makeTable(
          makeRow("Discount rate", outDiscountRate),
          makeRow("Discount amount", outDiscountAmount),
          makeRow("Discounted price", outDiscountedPrice)),
        makeTable(
          makeRow("Tax rate", outTaxRate),
          makeRow("Tax amount", outTaxAmount),
          makeRow("Final price", outFinalPrice))
      ).render
  }
}