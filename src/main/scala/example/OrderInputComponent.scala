package example

import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{BackendScope, ReactEventFromInput, ScalaComponent}

import scala.util.Try

object OrderInputComponent {

  class Backend(bs: BackendScope[_, Order]) {
    def onItemsChange(e: ReactEventFromInput) =
      e.extract(_.target.value)(value =>
        Try(value.toInt).toOption.map(newItems => bs.modState(_.copy(items = newItems))))

    def onPriceChange(e: ReactEventFromInput) =
      e.extract(_.target.value)(value =>
        Try(value.toDouble).toOption.map(newPrice => bs.modState(_.copy(price = newPrice))))

  }

  val OrderInput =
    ScalaComponent
      .builder[(Order, Backend)]("order input")
      .render_P {
        case (o, b) =>
          <.form(
            <.input.text(^.placeholder := "Items", ^.value := o.items, ^.onChange ==>? b.onItemsChange),
            <.input.text(^.placeholder := "Price", ^.value := o.price, ^.onChange ==>? b.onPriceChange)
          )
      }
      .build

  val ComputeOrder = ScalaComponent
    .builder[Order]("ComputeOrder")
    .initialState(Order(1, 1))
    .backend(new Backend(_))
    .renderPS(
      ($, _, o) =>
        <.div(
          OrderInput((o, $.backend)),
          <.p(o.price * o.items)
      ))
    .build
}
