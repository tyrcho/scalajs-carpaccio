package example

import org.scalajs.dom.html._
import org.scalajs.dom.raw._

import scala.util.Try
import scalatags.JsDom.all._

object InView {
  def render = {
    div(
      div(`class` := "form-group",
        label(`for` := "inItems", `class` := "col-sm-2 control-label", "Items"),
        div(`class` := "col-sm-10", inItems)),
      div(`class` := "form-group",
        label(`for` := "inPrice", `class` := "col-sm-2 control-label", "Unit Price"),
        div(`class` := "col-sm-10", inPrice)),
      div(`class` := "form-group",
        label(`for` := "inState", `class` := "col-sm-2 control-label", "Tax State"),
        div(`class` := "col-sm-10", inState)),
      div(`class` := "form-group",
        div(`class` := "col-sm-offset-2 col-sm-10", go))
    ).render
  }

  def validate(): Unit = go.click()

  def onValidate(f: Order => Unit): Unit = {
    def doIt(): Unit = readOrder.foreach(f)

    go.onclick = (_: MouseEvent) => doIt()
    inItems.onkeyup = (e: KeyboardEvent) => if (e.keyCode == 13) doIt()
    inPrice.onkeyup = (e: KeyboardEvent) => if (e.keyCode == 13) doIt()
    inState.onchange = (e: Event) => doIt()
  }

  private def readOrder: Try[Order] = {
    Try {
      val items = inItems.value.toInt
      val price = inPrice.value.toDouble
      val state = State.withName(inState.value)
      Order(items, price, state)
    }
  }

  private val inItems: Input = input(
    id := "inItems",
    `type` := "text",
    value := "10",
    placeholder := "#Items"
  ).render

  private val inPrice: Input = input(
    id := "inPrice",
    `type` := "text",
    value := "1000",
    placeholder := "Price for each item"
  ).render

  private val inState: Select = select(
    id := "inState",
    for (state <- State.values.toSeq) yield
      option(value := state.toString, state.description)
  ).render

  private val go: Button = button("Go!", `class` := "btn btn-primary").render


}
