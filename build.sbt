enablePlugins(ScalaJSBundlerPlugin)

name := "Elephant Carpaccio"

version := "0.1-SNAPSHOT"

scalaVersion := "2.12.7"

scalaJSUseMainModuleInitializer := true

libraryDependencies ++= Seq(
  "org.scala-js"                      %%% "scalajs-dom"       % "0.9.6",
  "com.lihaoyi"                       %%% "scalatags"         % "0.6.7",
  "com.github.karasiq"                %%% "scalajs-bootstrap" % "2.2.1",
  "com.github.japgolly.scalajs-react" %%% "core"              % "1.3.1",
  "org.scalatest"                     %% "scalatest"          % "3.0.5" % Test
)

npmDependencies in Compile ++= Seq(
  "react"     -> "16.5.1",
  "react-dom" -> "16.5.1"
)
