addSbtPlugin("org.scala-js"    % "sbt-scalajs"         % "0.6.25")
addSbtPlugin("ch.epfl.scala"   % "sbt-scalajs-bundler" % "0.13.1")
addSbtPlugin("io.get-coursier" % "sbt-coursier"        % "1.0.3")
